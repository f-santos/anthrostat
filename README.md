anthrostat
==========
[![pipeline status](https://gitlab.com/f.santos/anthrostat/badges/master/pipeline.svg)](https://gitlab.com/f.santos/anthrostat/commits/master)
[![coverage report](https://gitlab.com/f.santos/anthrostat/badges/master/coverage.svg)](https://gitlab.com/f.santos/anthrostat/commits/master)

## Features
`anthrostat` is an R package that aims to provide some useful functions for data analysis in past sciences, and specifically biological anthropology. It is still at an early stage of development and will receive regular updates, but it currently includes R functions for:

* graphical inspection of univariate outliers using robust location and scale estimators (`norm_outliers()`)
* graphical inspection of intra- and inter-observer measurement error (`plot_ccc()`, `plot_icc()`)
* reading files from Avizo (`read_avizo()`) and Viewbox (`read_viewbox()`)
* data manipulation and exploration (`remove_na()`, `count_na()`, `detect_rare_levels()`)
	
Contributions are welcome and encouraged (a contributing guide is available).

## Installation of the R package `anthrostat` from GitLab, using `remotes`

#### Install prerequisites

1. Make sure that [Git](https://git-scm.com/) and a [recent version of R](https://cran.r-project.org/) (newer than 4.0.0) are installed.

2. Install the R package `remotes` by typing the following command line into the R console:
   ```r
   install.packages("remotes", dep = TRUE)
   ```

3. Install build environment:
    * **Linux**: no additional operation required.
    * **OSX**: install *[XCODE](https://developer.apple.com/xcode/)*.
    * **Windows**: install the latest version of *[Rtools](https://cran.r-project.org/bin/windows/Rtools/)*. In particular, make sure to follow the steps of the section "Putting Rtools on the PATH" from the help page.

#### Installing anthrostat

Run the following command in R:
```r
remotes::install_git('https://gitlab.com/f-santos/anthrostat.git')
```
        

