## Create artificial data:
rater1 <- rnorm(15, 30, 5)
rater2 <- rater1 + rnorm(15)
rater3 <- rater1 + rnorm(15, 0, 1.5)
dat <- data.frame(rater1, rater2, rater3)

## Tests:
test_that("Data problems are detected", {
    expect_error(plot_icc(data = as.numeric(as.matrix(dat))),
                 "data should be a either a matrix or dataframe.")
    expect_error(plot_icc(as.matrix(dat[, 1])),
                 "data should have at least two columns.")
})

test_that("Names problems are detected", {
    expect_error(plot_icc(dat, raters_names = "toto"),
                   "The length of raters_name should be equal to the number of columns of data.")
})
