## Data:
x <- c(15, 14, 12, 8, 16, 13, 10, 17, 4, 8, 11, 6, 8, 7, 12)
y <- c(14, 13, 12, 9, 15, 13, 12, 15, 4, 8, 10, 7, 13, 8, 11)
names <- paste("Ind", seq_along(x))

## Tests:
test_that("Lengths problems are detected", {
    expect_error(plot_ccc(x, y[-1]),
                 "x and y cannot have different lengths.")
    expect_error(plot_ccc(x[-1], y),
                 "x and y cannot have different lengths.")
})

test_that("Names problems are detected", {
    expect_error(plot_ccc(x, y, names = names[-1]),
                   "names and x cannot have different lengths.")
    expect_warning(plot_ccc(x, y, id = 25),
                   "id cannot be greater that the number of individuals.")
})
