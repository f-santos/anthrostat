\name{summary.anthrostat_norm_outl}
\alias{summary.anthrostat_norm_outl}
\title{
  Display a summary of the results from four methods of univariate
  outlier detection
}
\description{
  This function allows a better display of the results from the function
  \code{\link{norm_outliers}}. The results from the four methods
  implemented are presented in a dataframe that should be immediately
  suitable for scientific publications.
}
\usage{
\method{summary}{anthrostat_norm_outl}(object, digits, ...)
}
\arguments{
  \item{object}{An object of class \code{anthrostat_norm_outl},
    typically returned by the function \code{\link{norm_outliers}}.}
  \item{digits}{The number of digits displayed in the table.}
  \item{...}{For consistency with \code{summary.default}, probably not
    of much use here.}
}
\value{
  A dataframe with the following columns:
  \item{Location}{Sample estimate for the location parameter (either
    mean or median here).}
  \item{Scale}{Sample estimate for the scale (i.e., dispersion)
    parameter.}
  \item{Coef}{Usually 2 or 3. The user-specified value for the parameter
    \eqn{k} in the definition of the intervals (see help of
    \code{\link{norm_outliers}}).}
  \item{Lower bound}{Lower bound of the interval outside of which any
    individual is considered as an outlier.}
  \item{Upper bound}{Upper bound of this interval.}
  \item{Outliers}{The indices of the outliers excluded.}
}
\seealso{
  \code{\link{norm_outliers}}
}
\author{
  Frédéric Santos, \email{frederic.santos@u-bordeaux.fr}
}
\examples{
## Create a random sample:
set.seed(2019)
z <- rnorm(15, mean = 0, sd = 1)
## Add two outliers on the right tail:
z <- c(z, 8, 12)
## Compute the outliers:
outl_results <- norm_outliers(z)
## Display results in a table:
summary(outl_results)
}
\concept{outliers}
