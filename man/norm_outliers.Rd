\name{norm_outliers}
\alias{norm_outliers}
\title{
  Implements four classical methods of univariate outlier detection,
  suitable for normally distributed variables.
}
\description{
  Four criteria to detect univariate outliers are applied to a numeric
  vector. This function returns an object of class
  \code{anthrostat_norm_outl}, which is basically a list of lists. The
  results can then be better visualized and compared by using the generic
  functions \code{plot} or \code{summary} on such an object.
}
\usage{
norm_outliers(x, coef = 2, id = names(x))
}
\arguments{
  \item{x}{A numeric vector, possibly with missing values (see Note below).}
  \item{coef}{The user-defined coefficient \eqn{k} in the formula
    defining the intervals (see Details below).}
  \item{id}{An optional argument (character vector) for the names of the
    individuals. This argument is useless if \code{names(x)} is already
    non-\code{NULL}; it is above all provided for the cases where
    \code{x} is extracted from the column of a dataframe and has no
    names attached.}
}
\details{
  This function can be used on a numeric \eqn{x} for which the
  assumption of normality (disregarding some potential extreme values)
  seems to be reasonable.
  
  The four methods implemented compute an interval outside of which any
  individual will be considered as an outlier. Those intervals are of
  the general form:

  \eqn{[m - k \times s; m + k \times s]}

  where \eqn{m} is a location sample estimate, \eqn{s} is a scale (i.e.,
  dispersion) estimate, and \eqn{k} is a user-defined constant (usually
  lying between 2 and 3). If \eqn{k = 3}, then the intervals are wider
  and only very clear outliers will be detected. If \eqn{k = 2}, then
  the intervals are narrower and even slightly suspicious values can be
  excluded.

  The four methods differ only in their choice of location and scale estimates:
  \itemize{
    \item \code{mean_std}: implements the "95-99.7" rule, i.e. use the sample
      mean as the location estimate \eqn{m}, and the standard deviation as
      the location estimate \eqn{s}.
    \item \code{med_iqr}: use the sample median as the location estimate
      \eqn{m}, and \code{IQR(x)/1.349} as the location estimate \eqn{s}.
    \item \code{med_mad}: use the sample median as the location estimate
      \eqn{m}, and the median absolute deviation \code{mad(x)} as the
      location estimate \eqn{s}.
    \item \code{med_sn}: use the sample median as the location estimate
      \eqn{m}, and the \eqn{S_n} estimator \code{robustbase::Sn(x)} as
      the location estimate \eqn{s}.
  }
}
\value{
  An object of the \emph{ad-hoc} class \code{anthrostat_norm_outl},
  which is basically a list of lists. This object is a list of five
  components: a self-explanatory component \code{data}, and four
  components \code{mean_std}, \code{med_iqr}, \code{med_mad} and
  \code{med_sn} corresponding to each of the four methods described
  below. Each of those components is itself a list with the following
  components:
  \itemize{
    \item \code{location}: the location estimate for the given method
    \item \code{scale}: the scale estimate for the given method
    \item \code{coef}: the user-defined value \code{coef} in this
    function
    \item \code{lower_bound}: the lower bound of the interval defined
    above
    \item \code{upper_bound}: the upper bound of the interval defined
    above
    \item \code{outliers}: either the names or indices of the
      individuals detected as outliers in \code{x}
  }
}
\note{
  Missing values are removed. It is advisable to provide individuals'
  names, either directly in \code{x} and through the argument \code{id},
  so as to make the results more readable.
}
\references{
  Rousseeuw, P.J. and Croux, C. (1993) Alternatives to the Median Absolute
  Deviation, \emph{Journal of the American Statistical Association}
  \bold{88}, 1273-1283. doi: 10.1080/01621459.1993.10476408

  Leys, C., Ley, C., Klein, O., Bernard, P. and Licata, L. (2013)
  Detecting outliers: Do not use standard deviation around the mean, use
  absolute deviation around the median. \emph{Journal of Experimental
    Social Psychology} \bold{49}, 764–766. doi: 10.1016/j.jesp.2013.03.013

  Lightfoot, E. and O'Connell, T. C. (2016) On the Use of Biomineral
  Oxygen Isotope Data to Identify Human Migrants in the Archaeological
  Record: Intra-Sample Variation, Statistical Methods and Geographical
  Considerations. \emph{PLOS ONE} \bold{11}, e0153850. doi:
  10.1371/journal.pone.0153850
}
\author{
  Frédéric Santos, \email{frederic.santos@u-bordeaux.fr}
}
\seealso{
  \code{robustbase::Sn}, \code{stats::mad}
}
\examples{
## Create a random sample:
set.seed(2019)
z <- rnorm(15, mean = 0, sd = 1)
## Add two outliers on the right tail:
z <- c(z, 8, 12)
## Compute the outliers:
outl_results <- norm_outliers(z)
## Display results in a table:
summary(outl_results)
}
\concept{outliers}
