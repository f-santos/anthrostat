# anthrostat 0.1.6 (Release date: 2020-11-17)

## New functions
* Added function: `read_avizo()`.


# anthrostat 0.1.6 (Release date: 2020-03-12)

## New functions
* Added function: `inertia()` and `decomp_inertia()`.


# anthrostat 0.1.5 (Release date: 2020-02-06)

## New functions
* Added function: `count_na()`, `count_nonmissing()` and `remove_na()`.

# anthrostat 0.1.4 (Release date: 2020-01-21)

## Minor changes
* Included reference sample of DSP sex estimation method
* Minor code improvements


# anthrostat 0.1.3 (Release date: 2020-01-17)

## Bug fix
* Fixed error message in `plot.anthrostat_norm_outliers()` when no outlier was present and `number_id = -1` was specified.

## Other changes
* Added contributing guide


# anthrostat 0.1.2 (Release date: 2019-12-14)

## New features
* More customization options for `plot_ccc()` function
* New function `plot_icc()`
* New dataset `C1_interobs` included in the package


# anthrostat 0.1.1 (Release date: 2019-12-11)

## New features
* New function `plot_ccc()`

## Minor changes
* Various improvements in documentation files
* `anthrostat`now imports `epiR` and `plotrix` R packages


# anthrostat 0.1.0 (Release date: 2019-09-23)

First stable release on GitLab
