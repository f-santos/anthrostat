Contributing to `anthrostat` development
========================================

This outlines how to propose various changes or improvements to `anthrostat`.
All contributions are welcome and encouraged.

## Fixing typos

The easiest way to contribute consists in fixing typos or grammatical errors in documentation files (and there might be quite a lot of them, since they are written by a non-native English speaker!).
Fixes can be made through the GitLab web interface directly in the `.Rd` files (`anthrostat` does not use `roxygen` and no documentation is included in the source files).

## Pull requets

More substantial changes can be made through pull requests.

### Prerequisites

Before making a pull request, please file an issue first.
Any bug report should include a minimal reproducible example.

### Pull request process

*  It is better to create a Git branch for each pull request.
*  No precise style guide is required to add new R code. However, please don't restyle or modify code which is unrelated with your pull request.
*  `anthrostat` implements unit tests using [testthat](https://cran.r-project.org/package=testthat). Contributions with test cases included would be easier to review (but this is not mandatory).
