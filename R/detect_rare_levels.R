detect_rare_levels <- function(data,
                               threshold = max(1, floor(0.05*nrow(data)))) {
### data: a dataframe, with at least one factor
### threshold: a factor level is considered as "rare" if it is observed
###            on 'threshold' individuals at most.
### Returns the names of the factors of 'data' with at least one rare level.

##############
### 0. Tests :
    ## Check arguments:
    if (!is.data.frame(data)) {
        stop("data should be a dataframe")
    }
    types <- unlist(lapply(data, class)) # types of each column
    if (all(types != "factor")) {
        stop("data should contain at least one factor")
    }
    if (threshold <= 0) {
        stop("threshold must be stricly positive")
    }
    if (threshold != floor(threshold)) {
        warning("threshold must be an integer. Its rounded value has been used.")
        threshold <- round(threshold)
    }

##############################################
### 1. Detection of factors with rare levels :
    data <- data[, types == "factor"] # keep only the factors
    is_rare <- rep(FALSE, ncol(data)) # initialize empty vector
    for (j in seq_along(data)) { # for each factor (i.e., column) in the dataframe
        if (min(table(data[, j])) <= threshold) {
            is_rare[j] <- TRUE
        }
    }

#####################
### 2. Return value :
    return(colnames(data)[is_rare])
}
